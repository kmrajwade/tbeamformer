#!/usr/env/python
from __future__ import print_function
import numpy as np
import logging, glob, sys
import argparse
import matplotlib.pyplot as plt
from fbfuse_weights_generator import *
from astropy.time import Time
import logging
import argparse
import time
import tornado.gen
from builtins import range
from datetime import datetime
from io import StringIO
import json
from astropy import units as u
from scipy import linalg




# Argument list
parser = argparse.ArgumentParser(description = "A script to generate coherent beams")
parser.add_argument("-f", "--freq", action='store', type=int, dest='freq', required=True,
    help="Number of frequency channels per subband")
parser.add_argument("-x", "--start", action='store', type=int, dest='start', required=True,
    help="start time for the time range for sensor history (unix time)")
parser.add_argument("-y", "--end", action='store', type=int, dest='end', required=True,
    help="end time for the time range for sensor history (unix time)")
parser.add_argument("-t", "--total", action='store', type=int, dest='nchans', required=True,
    help="Total number of channels")
parser.add_argument("-a", "--antlist", action='store', type=str, dest='antlist', required=True,
    help="File containing a list of antenna index from mkrecv header (a comma separated list)")
parser.add_argument("-p", "--npol", action='store', type=int, dest='npol', required=True,
    help="Number of polarizations")
parser.add_argument("-s", "--nsamps", action='store', type=int, dest='nsamps', required=True,
    help="Number of time samples")
parser.add_argument("-l", "--lowfreq", action='store', type=float, dest='lowfreq', required=True,
    help="Lower edge of the frequency band (Hz)")
parser.add_argument("-b", "--bandwidth", action='store', type=float, dest='bw', required=True,
    help="Bandwidth (Hz)")
parser.add_argument("-n", "--nbeams", action='store', type=int, dest='nb', required=True,
    help="Number of beams to form")
parser.add_argument("-c", "--centre", action='store', type=str, dest='cen', required=True,
    help="RA and DEC (hh:mm:ss.ss, +-dd:mm:ss.ss) of the phase centre with beams")
parser.add_argument("-o", "--burstpos", action='store', type=str, dest='burstpos', required=True,
    help="RA and DEC (hh:mm:ss.ss, +-dd:mm:ss.ss) of the FRB location")
parser.add_argument("-e", "--epoch", action='store', type=str, dest='epoch', required=True,
    help="epoch (yyy-mm-dd hh:mm:ss.ss) to compute the delays")
parser.add_argument('-sensors', metavar='sensor', nargs='+',
    help="list of sensor names or filter strings to request data for")
parser.add_argument('--host', default='127.0.0.1', 
    help="hostname or IP of the portal server (default: %(default)s).")
parser.add_argument('-i', '--include-value-time', dest="include_value_time", action="store_false",
    help="include value timestamp")
parser.add_argument('-m', '--dispersion-measure', dest="dm", required=True,
    help="dispersion measure of the the burst")
parser.add_argument('--jones' ,action="store_true",
    help="flag to correct for off-boresite polarization")
parser.add_argument(
        '-d', '--decimate',
        type=int,
        metavar='N',
        default=1,
        help="decimation level - only every Nth sample is output (default: %(default)s).")
 


args = parser.parse_args()
if args.jones:
    jones_matrix = np.load("jones_matrix.np", encoding='latin1', allow_pickle=True)

from katportalclient import KATPortalClient

@tornado.gen.coroutine
def get_antenna_list():
    # Change URL to point to a valid portal node.
    # If you are not interested in any subarray specific information
    # (e.g. schedule blocks), then the number can be omitted, as below.
    # Note: if on_update_callback is set to None, then we cannot use the
    #       KATPortalClient.connect() and subscribe() methods here.
    portal_client = KATPortalClient('http://{host}/api/client'.format(**vars(args)),
                                    on_update_callback=None)

    # Get the names of sensors matching the patterns
    sensor_names = yield portal_client.sensor_names(args.sensors)
    print("\nMatching sensor names: {}".format(sensor_names))
    # Example output (if sensors is 'm01[12]_pos_request_base'):
    #   Matching sensor names: [u'm011_pos_request_base_azim',
    #   u'm012_pos_request_base_ra', u'm012_pos_request_base_dec',
    #   u'm011_pos_request_base_ra', u'm012_pos_request_base_elev',
    #   u'm011_pos_request_base_dec', u'm012_pos_request_base_azim',
    #   u'm011_pos_request_base_elev']

    # Fetch the details for the sensors found.
    for sensor_name in sensor_names:
        sensor_detail = yield portal_client.sensor_detail(sensor_name)
        print("\nDetail for sensor {}:".format(sensor_name))
        for key in sorted(sensor_detail):
            print("    {}: {}".format(key, sensor_detail[key]))
        # Example output:
        #   Detail for sensor m011_pos_request_base_azim:
        #       component: m011
        #       description: Requested target azimuth
        #       katcp_name: m011.pos.request-base-azim
        #       name: m011_pos_request_base_azim
        #       params: [-195.0, 370.0]
        #       site: deva
        #       systype: mkat
        #       type: float
        #       units: deg

    num_sensors = len(sensor_names)
    if num_sensors == 0:
        print("\nNo matching sensors found - no history to request!")
    else:
        print ("\nRequesting history for {} sensors, from {} to {}"
               .format(
                   num_sensors,
                   datetime.utcfromtimestamp(
                       args.start).strftime('%Y-%m-%dT%H:%M:%SZ'),
                   datetime.utcfromtimestamp(args.end).strftime('%Y-%m-%dT%H:%M:%SZ')))
        value_time = args.include_value_time
        if len(sensor_names) == 1:
            # Request history for just a single sensor - result is
            # sample_time, value, status
            #    If value timestamp is also required, then add the additional argument:
            #        include_value_time=True
            #    result is then sample_time, value_time, value, status
            history = yield portal_client.sensor_history(
                sensor_names[0], args.start, args.end,
                include_value_ts=value_time)
            histories = {sensor_names[0]: history}
        else:
            # Request history for all the sensors - result is sample_time, value, status
            #    If value timestamp is also required, then add the additional argument:
            #        include_value_time=True
            #    result is then sample_time, value_time, value, status
            histories = yield portal_client.sensors_histories(sensor_names, args.start,
                                                              args.end,
                                                              include_value_ts=value_time)
        print("Found {} sensors.".format(len(histories)))
        for sensor_name, history in list(histories.items()):
            num_samples = len(history)
            print("History for: {} ({} samples)".format(sensor_name, num_samples))
            if num_samples > 0:
                for count in range(0, num_samples, args.decimate):
                    item = history[count]
                    if count == 0:
                        print("\tindex,{}".format(",".join(item._fields)))
                    print("\t{},{}".format(count, item.csv()))


def tokenize(data, delim):
    tokens = []
    current_token = []
    json_depth = 0
    for char in data:
        if char == delim and json_depth <= 0:
            tokens.append("".join(current_token))
            current_token = []
        elif char == "{":
            json_depth += 1
            current_token.append(char)
        elif char == "}":
            json_depth -= 1
            current_token.append(char)
        else:
            current_token.append(char)
    else:
        tokens.append("".join(current_token))
    return tokens


    

# make beams depending on the weights provided
def makebeams(weights, beamnum):
# Map order in the TB to the actual antenna list in the CBF
   ind = 0

   logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)
   for ant_id in tb_index:
  
  #string = str(int(ant_id)) + "h"
       ant_order[ind] = ant_id 
       ind +=1

# Get all files

   files = sorted(glob.glob("*.dat"))
   solutions = sorted(glob.glob("*.npy"))
   subbandids = np.zeros(len(files))

# Apply Gain solution
   coherent_beams = np.zeros((1,int(args.nsamps), 2), dtype='complex64')
   for ll in range(len(files)):
       finput = open(files[ll], "rb")
       header_bytes = finput.read(4096)
       header = {i.split()[0]:i.split()[1:] for i in header_bytes.splitlines() if len(i.split()) > 1}
       cen_freq = float(header[b'FREQ'][0])
       subband_id = int((cen_freq - args.lowfreq)/((args.bw/args.nchans)*args.freq))
       subbandids[ll] = subband_id
       print("processing file %s with solutions %s", files[ll], solutions[ll])
       print("Processing data for Subband ID: %d", subband_id)
       solns = np.load(solutions[ll])
       solns_fin = np.transpose(solns, (1,0,2)) # FAP order
# Add the proper correction
# repeat solutions for both polarisations
       for i in range(2):
           solns_fin[:,:,i] = solns_fin[:,:,i] * weights[beamnum,args.freq*subband_id:(subband_id + 1)*args.freq,:]  #beam frequency antenna order 
# Getting rid of outer antennas
       #solns_fin[:,3:22,:] = 0. 
       tb_data = np.reshape(np.fromfile(finput, dtype = 'byte'),(freq, int(len(tb_index)), args.nsamps, args.npol,2))
       tb_data = tb_data.astype("float32").view("complex64").squeeze()

# Flip the frequency order of the gain solutions
       logging.info("Applying gain solutions...")
       for jj in range(args.nsamps):
           tb_data[:,:,jj,:] = tb_data[:,:,jj,:]*solns_fin

 # Calibrating Polarizations with Jones matrices
       if (args.jones):
           logging.info("Applying Poln cal with Jones matrix multiplication...")
           for i in range(len(tb_index)): 
             for j in range(freq):
               E_field = np.matrix([tb_data[j,i,:,0], tb_data[j,i,:,1]])
               E_true = np.dot(linalg.inv(jones_matrix[j,i]), E_field)
               tb_data[j,i,:,0] = E_true[0,:]
               tb_data[j,i,:,1] = E_true[1,:]

 
# Generate coherent beams and save only the bandpass
       coherent_complex = tb_data.sum(axis=1)
       coherent_beams = np.vstack((coherent_beams, coherent_complex[:,:,:]))

# Save the new tb_data file
       finput.close()
   
   np.savetxt("subbandlist.txt", subbandids, fmt='%d') 
   logging.info("Opening %s for writing..","beam" + str(beamnum) + "_phased")
   np.savez("beam" + str(beamnum)+"_phased"+".npz", coherent_beams[1::,:,:].astype("complex64"))
# Run the full code
if __name__ == "__main__":

    logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)
    freq = int(args.freq)

    tb_index = np.loadtxt(args.antlist, delimiter=',')
    ant_order = np.zeros(len(tb_index), dtype=int)

    # get antenna list from the sensors
    args = parser.parse_args()

    # Start up the tornado IO loop.
    # Only a single function to run once, so use run_sync() instead of start()

    io_loop = tornado.ioloop.IOLoop.current()
    tmp = sys.stdout
    ANTENNAS = StringIO()
    sys.stdout  = ANTENNAS
    io_loop.run_sync(get_antenna_list)    
    sys.stdout = tmp
    antennas = json.loads(tokenize(ANTENNAS.getvalue(), ",")[8])
   
    
    # get the phase centre (TBD)
    sys.stdout.flush()

    antenna_ids = sorted(antennas.keys())
    print("Original antenna ids:",antenna_ids)

    # get the antenna list in FBFUSE order
    f = open(args.antlist)
    fbfuse_antennas = f.readlines()
    fbfuse_antlist =  fbfuse_antennas[0].split('\n')[0].split(",")
    final_list = []
    for i in range(len(fbfuse_antlist)):
       index = int(fbfuse_antlist[i])
       final_list.append(antenna_ids[index])

    print("ordered antenna ids:",final_list)
    # convert antennas to katpoint.Antenna objects
    antennas_fin = [Antenna(antennas[name]) for name in final_list]
    f.close()


    # define boresight pointing position (array phase centre)
    phase_centre = Target("boresight, radec," + str(args.cen))
    burst_position = Target("burstpos, radec," + str(args.burstpos))

    # get the reference antenna (usually passed by CAM and available in the mXXX_obeserver sensor strings)
    reference_antenna = antennas_fin[0].array_reference_antenna()

    # define the reference frequency to use for the PSF generation when tiling
    reference_frequency = (args.lowfreq + (args.bw/2.0))/1e6 * u.megahertz

    # create an array configuration
    config_frb = ArrayConfiguration(antennas_fin, burst_position, reference_antenna, reference_frequency)

    # specify the observation epoch (using current time as an example)
    burst_epoch = Time(args.epoch, scale="utc", format='iso')

    # define the frequencies to generate weights for
    frequencies = np.linspace(args.lowfreq, args.lowfreq + args.bw, args.nchans) * u.Hz

    #create epochs per freq channel
    burst_epochs = create_epochs_per_frequency(burst_epoch, frequencies, args.dm)

    # specify the desired number of beams in the tiling
    nbeams = args.nb

    # generate the tiling of 1000 beams out to a radius of 0.1 degrees
    tiling_frb = make_circular_tiling(config_frb, burst_position, burst_epoch, nbeams)
    print("Beam co-ordinates are: {}".format(tiling_frb.get_equatorial_coordinates()))

    # check to see the acheived number of beams
    print("Nbeams: {}".format(tiling_frb.beam_num))

    # check to see the acheived beam overlap
    print("Beam overlap: {:.5f}".format(tiling_frb.overlap))

    # create a set of delay models for all beams and antennas
    delays = create_delay_solutions(config_frb, tiling_frb, phase_centre, burst_epochs)

    # calculate the weights at the specified epoch
    weights = delay_to_phase(delays, frequencies, tiling_frb.beam_num, burst_epochs, len(antennas_fin))

    # make all the beams
    for kk in range (args.nb):
      makebeams(weights, kk)
