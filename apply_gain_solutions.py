#!/usr/env/python
import katdal
import numpy as np
from katsdpcal.plotting import *
from katsdpcal.report import utc_tstr
from katsdpcal.calprocs_dask import wavg
import dask.array as da
import logging, glob
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="A script to apply gain solutions to the TB data. The solutions are accessed from the katdal archive. One needs the observation token in order to use this script.")
parser.add_argument("-f", "--freq", action='store', type=int, dest='freq', required=True,
    help="Number of frequency channels per subband")
parser.add_argument("-t", "--total", action='store', type=int, dest='nchans', required=True,
    help="Total number of channels")
parser.add_argument("-a", "--antlist", action='store', type=str, dest='antlist', required=True,
    help="File containing a list of antenna index from mkrecv header (a comma separated list)")
parser.add_argument("-p", "--npol", action='store', type=int, dest='npol', required=True,
    help="Number of polarizations")
parser.add_argument("-s", "--nsamps", action='store', type=int, dest='nsamps', required=True,
    help="Number of time samples")
parser.add_argument("-l", "--lowfreq", action='store', type=float, dest='lowfreq', required=True,
    help="Lower edge of the frequency band (Hz)")
parser.add_argument("-b", "--bandwidth", action='store', type=float, dest='bw', required=True,
    help="Bandwidth (Hz)")
parser.add_argument("-u", "--unix", action='store', type=int, dest='unix', required=True,
    help="Unix timestamp of when the solutions were computed")


args = parser.parse_args()

def get_gain_solutions(ts_attrs):
        hv_delays = ts_attrs.get(str(args.unix) + '_cal_product_KCROSS_DIODE')

        delays = ts_attrs.get(str(args.unix) + '_cal_product_K')

        bp_gains = ts_attrs.get(str(args.unix) + '_cal_product_B0')

        hv_gains = ts_attrs.get(str(args.unix) + '_cal_product_BCROSS_DIODE_SKY0')

        gains = ts_attrs.get(str(args.unix) + '_cal_product_G')

        delays = delays + np.nan_to_num(hv_delays, copy=False, nan=0.0)

        bp_gains = bp_gains * np.nan_to_num(hv_gains, copy=False, nan=1.0)

        pol=ts_attrs['cal_pol_ordering']
        antenna_names=ts_attrs['cal_antlist']
        n_chans = ts_attrs.get('cal_n_chans')
        bandwidth = ts_attrs.get('cal_bandwidth')
        centre_freq = ts_attrs.get('cal_center_freq')
        cal_freqs = centre_freq + (np.arange(n_chans) - n_chans / 2) * (bandwidth / n_chans)

        average_gain = np.zeros((len(pol) *len(antenna_names)))
        gain_corrections = np.zeros((len(cal_freqs), len(pol), len(antenna_names)),dtype='complex64')

        ind = 0
        for i in range(len(antenna_names)):

            for j in range(len(pol)):

                    K_gains = np.exp(-2j * np.pi * delays[j,i] * cal_freqs)
                    final_gains = K_gains * bp_gains[:,j,i] * gains[j,i]
                    abs_gains = np.abs(final_gains)
                    average_gain[ind] = np.nanmedian(abs_gains)
                    corrections = 1.0 / final_gains
                    corrections *= abs_gains / average_gain[ind]
                    gain_corrections[:,j,i] = np.nan_to_num(corrections)
                    ind +=1

        valid_average_gains = [g for g in average_gain.flatten() if g > 0]
        global_average_gain = np.median(valid_average_gains)

        ind = 0
        for i in range(len(antenna_names)):

            for j in range(len(pol)):
                    relative_gain = average_gain[ind] / global_average_gain        
                    gain_corrections[:,j,i] *= global_average_gain
                    safe_relative_gain = np.clip(relative_gain, 0.5, 2.0)
                    if (relative_gain == safe_relative_gain):
                        logging.info("%s: average gain relative to global average = %5.2f",
				  i, relative_gain)
                    else:
                        logging.warning("%s: average gain relative to global average "
							"= %5.2f out of range, clipped to %.1f",
							i, relative_gain, safe_relative_gain) 

                        gain_corrections[:,j,i] *= relative_gain / safe_relative_gain
                    ind += 1

        return gain_corrections, cal_freqs 

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)

f = katdal.open('https://archive-gw-1.kat.ac.za/1617367872/1617367872_sdp_l0.full.rdb?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiJ9.eyJpc3MiOiJrYXQtYXJjaGl2ZS5rYXQuYWMuemEiLCJhdWQiOiJhcmNoaXZlLWd3LTEua2F0LmFjLnphIiwiaWF0IjoxNjIzMzEyNDU4LCJwcmVmaXgiOlsiMTYxNzM2Nzg3MiJdLCJleHAiOjE2MjM5MTcyNTgsInN1YiI6Im1hbmlzaGEuY2FsZWJAbWFuY2hlc3Rlci5hYy51ayIsInNjb3BlcyI6WyJyZWFkIl19.9hblwwH4RpiATUF1l5S_VNRgjUKOde-5Iwy4ykpY5v-mOv32UhNP7hiuAyI7O5Bab0lG_1256OT94yC3qRm7pw')
freq = int(args.freq)


ts_attrs=f.source.metadata.attrs
ant_index=ts_attrs['cbf_input_labels']
tb_index = np.loadtxt(args.antlist, delimiter=',')
ant_order = np.zeros(len(tb_index), dtype=int)
gain_corrections, cal_freqs = get_gain_solutions(ts_attrs)

# Map order in the TB to the actual antenna list in the CBF

ind = 0
for ant_id in tb_index:
  ant_order[ind] = ant_id 
  ind +=1

# Get all files
files = sorted(glob.glob("*.dat"))
correct_id = 0
fileindex = 0
iter = 0

# Apply Gain solution
for ll in range(len(files)):
  fileindex = ll - iter
  iter = 0
  finput = open(files[fileindex], "rb")
  header_bytes = finput.read(4096)
  header = {i.split()[0]:i.split()[1:] for i in header_bytes.splitlines() if len(i.split()) > 1}
  cen_freq = float(header[b'FREQ'][0])
  subband_id = int((cen_freq - args.lowfreq)/((args.bw/args.nchans)*args.freq))
  logging.info("Subband ID: %d, Correct ID: %d", subband_id, correct_id)

# Choose the right set of frequencies
  while (subband_id != correct_id):
    finput.close()
    fileindex += 1
    iter += 1
    finput = open(files[fileindex], "rb")
    header_bytes = finput.read(4096)
    header = {i.split()[0]:i.split()[1:] for i in header_bytes.splitlines() if len(i.split()) > 1}
    cen_freq = float(header[b'FREQ'][0])
    subband_id = int((cen_freq - args.lowfreq)/((args.bw/args.nchans)*args.freq))

  correct_id += 1
  logging.info("Processing data for Subband ID: %d", subband_id)
  logging.info("Selecting gain solutions corresponding to %f MHz",cen_freq/1e6)
  
  tb_data = np.reshape(np.fromfile(finput, dtype = 'byte'),(freq, int(len(tb_index)), args.nsamps, args.npol,2))
  tb_data = tb_data.astype("float32").view("complex64")

# Flip the frequency order of the gain solutions
  corrections  = gain_corrections[int(subband_id*args.freq): int(subband_id*args.freq) + args.freq,:,:]

# Account for inverted pol ordering in the gain solutions
  pol_order = np.array([1,0])

  logging.info("Applying gain solutions...")

  for ii in range(freq):
     
    for jj in range(int(len(tb_index))):

       for kk in range(args.npol):
   
         tb_data[ii,jj,:,kk] *= corrections[ii,pol_order[kk],ant_order[jj]]
  
# Save the new tb_data file
  tb_data = tb_data.flatten().view('float32').astype('byte')
  logging.info("Opening %s for writing..", files[ll] + "_phased")
  foutput = open(files[ll] + "_phased", "wb")
  foutput.write(header_bytes)
  foutput.write(tb_data)
  foutput.close()
  finput.close()
