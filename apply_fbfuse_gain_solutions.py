#!/usr/env/python
import katdal
import numpy as np
from katsdpcal.plotting import *
from katsdpcal.report import utc_tstr
from katsdpcal.calprocs_dask import wavg
import dask.array as da
import logging, glob
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--freq", action='store', type=int, dest='freq',
    help="Number of frequency channels per subband")
parser.add_argument("-t", "--total", action='store', type=int, dest='nchans',
    help="Total number of channels")
parser.add_argument("-a", "--antlist", action='store', type=str, dest='antlist',
    help="File containing a list of antenna index from mkrecv header (a comma separated list)")
parser.add_argument("-p", "--npol", action='store', type=int, dest='npol',
    help="Number of polarizations")
parser.add_argument("-s", "--nsamps", action='store', type=int, dest='nsamps',
    help="Number of time samples")
parser.add_argument("-l", "--lowfreq", action='store', type=float, dest='lowfreq', required=True,
    help="Lower edge of the frequency band (Hz)")
parser.add_argument("-b", "--bandwidth", action='store', type=float, dest='bw', required=True,
    help="Bandwidth (Hz)")



logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)

args = parser.parse_args()

freq = int(args.freq)

tb_index = np.loadtxt(args.antlist, delimiter=',')
ant_order = np.zeros(len(tb_index), dtype=int)



# Map order in the TB to the actual antenna list in the CBF
ind = 0
for ant_id in tb_index:
  
  #string = str(int(ant_id)) + "h"
  ant_order[ind] = ant_id 
  ind +=1

# Get all files

files = sorted(glob.glob("*.dat"))
solutions = sorted(glob.glob("*.npy"))
ind =0
# Apply Gain solution
"""
  if (ll != 40 and ll !=41):
   finput = open(files[ll], "rb")
   if (ll > 41):
     ind = ll -2
     solns = np.load(solutions[ll-2])
   else:
     ind = ll
"""
for ll in range(len(files)):
   finput = open(files[ll], "rb")
   solns = np.load(solutions[ll])
   solns_fin = np.transpose(solns, (1,0,2))
   logging.info("processing file %s with solutions %s", files[ll], solutions[ll])
   header_bytes = finput.read(4096)
   header = {i.split()[0]:i.split()[1:] for i in header_bytes.splitlines() if len(i.split()) > 1}
  
# Choose the right set of frequencies
   cen_freq = float(header[b'FREQ'][0])
   subband_id = int((cen_freq - args.lowfreq)/((args.bw/args.nchans)*args.freq))
   logging.info("Processing data for Subband ID: %d", subband_id)
   logging.info("Selecting gain solutions corresponding to %f MHz",cen_freq/1e6)
  
   tb_data = np.reshape(np.fromfile(finput, dtype = 'byte'),(freq, int(len(tb_index)), args.nsamps, args.npol,2))
   tb_data = tb_data.astype("float32").view("complex64")

# Flip the frequency order of the gain solutions
  #correct_index = (np.abs(cal_freqs - cen_freq)).argmin()
  #corrections  = gain_corrections[correct_index-int(args.freq/2): correct_index + int(args.freq/2),:,:]
   logging.info("Applying gain solutions...")
   for jj in range(args.nsamps):
    tb_data[:,:,jj,:,0] = tb_data[:,:,jj,:,0]*solns_fin
  
# Save the new tb_data file
   tb_data = tb_data.flatten().view('float32').astype('byte')
   logging.info("Opening %s for writing..", files[ll] + "_phased")
   foutput = open(files[ll] + "_phased", "wb")
   foutput.write(header_bytes)
   foutput.write(tb_data)
   foutput.close()
   finput.close()
