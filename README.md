**TBeamformer**: A set of python scripts that apply the gain solutions to complex voltage data (MeerKAT) and generate coherent and incoherent beams t a user-defined location.

**Description**
There are seven main scripts:
1) `apply_gain_solutions.py`: This script accesses solution from the katdal archive and applies them to data. One needs to hknow the exact observation token for the same (needs to be manually added to script).
2) `apply_fbfuse_gain_solutions.py`: This script applies the solutions that are saved by FBFUSE. 
3) `gen_dyn_spec.py`:  Combines the phased data from all subbands to form coherent and incoherent beam.
4) `fbfuse_weights_generator.py`: All the classes required for generating beamformer weights for the coherent beams
5) `tb_coherent_dedispersion.py`: Script to remove intra-channel dispersion smear
6) `generate_coherent_beams.py`: This script will generate a coherent beam tile at the user-defined position
7) `get_antenna_list.py` : A script that extracts the antenna list by querying the CAM sensor at any given time

**Dependencies**
For the phasing up scripts, a list of all antennas is needed as an argument. This file can be obtained in the following way:
`head -c 4096 fbfpn00_numa1.dat|head -31|tail -1|awk '{print $2}' > ant_index.txt`

One also needs to install `katdal` (https://pypi.org/project/katdal/), `katsdpcal` (https://github.com/ska-sa/katsdpcal) and the dependencies therein.

For the coherent beamformer, one also needs `MOSAIC` (https://github.com/wchenastro/Mosaic)
