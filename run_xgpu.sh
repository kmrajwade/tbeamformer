#!/bin/bash

# exit on any error
set -e

# Help for the script
Help()
{
   # Display Help
   echo "A code that runs xGPU on the phased TB data"
   echo
   echo "Syntax: run_xgpu.sh  [t|a|f]"
   echo "options:"
   echo "t     number of time samples"
   echo "a     number of antennas."
   echo "v     number of frequency channels per subband."
}

# check the number of arguments
if [ $# -ne 3 ]; then 
    echo "illegal number of parameters"
    echo "usage: run_xgpu.sh <samples> <number of antennas> <number of subbands>"
    Help
    exit 1
fi

samples=$1
antennas=$2
nchans=$3

N=$(ls *.dat_phased|wc -l)

for ((ii=1; ii <= $N; ii++))
do
    datfile=$(ls *.dat_phased|head -$ii|tail -1)
    basefile=$(ls *.dat_phased|head -$ii|tail -1|cut -d. -f 1)
    if [ $3 -eq 64 ]; then
      /home/krajwade/MeerTRAP-fast_imaging/tb_transpose/build/tb_transpose/tb_pipeline -a ${antennas} -f ${nchans} -p 2 -s 4x${samples}x${antennas}x${nchans} -d 0,2,3,1 -i $datfile -o $basefile.corr -t ${samples}
    fi
   
    if [ $3 -eq 16 ]; then
      /usr/local/share/apps/src/MeerTRAP-fast_imaging_16/tb_transpose/build/tb_transpose/tb_pipeline -a ${antennas} -f ${nchans} -p 2 -s 4x${samples}x${antennas}x${nchans} -d 0,2,3,1 -i $datfile -o $basefile.corr -t ${samples}
    fi 
    if [ $3 -eq 512 ]; then
      /usr/local/share/apps/src/MeerTRAP-fast_imaging_512/tb_transpose/build/tb_transpose/tb_pipeline -a ${antennas} -f ${nchans} -p 2 -s 4x${samples}x${antennas}x${nchans} -d 0,2,3,1 -i $datfile -o $basefile.corr -t ${samples}
    fi
done
