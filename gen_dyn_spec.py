#/usr/env/python

import numpy as np
import matplotlib.pyplot as plt
import argparse
import glob
import logging

# Setting up logging config
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)

# Parse Arguments
parser = argparse.ArgumentParser(description='Generate an incoherent and coherent beam at the phase centre')
parser.add_argument('--nants', dest='nants', type=int,
                    help='Number of antennas', required=True)
parser.add_argument('--nchans', dest='nchans',type=int,
                    help='Number of frequency channels', required = True)
parser.add_argument('--wild', dest='wcard', type=str,
                     help='wild card of the file names', required = True)
parser.add_argument('--nsamps', dest='nsamps', type=int,
                     help='total number of time samples', required = True)
parser.add_argument('--basename', dest='basename', type=str,
                     help='Basename of the file to save the beams in', required = True)


args = parser.parse_args()

files = sorted(glob.glob("*."+ args.wcard))

f = open(files[0])
f.seek(4096)
y = np.fromfile(f, dtype='byte')
tsamps = y.size/(args.nchans*args.nants*2*2)
coherent_fin = np.zeros((1,int(tsamps/args.tscr), 2), dtype='complex64')
incoherent_fin = np.zeros((1,int(tsamps/args.tscr)), dtype='float32')
f.close()

for i in range(len(files)):
    f = open(files[i])
    f.seek(4096)
    
    logging.info("Generating coherent and incoherent beams....")
    x = np.fromfile(f, dtype='byte')
    x = x.reshape(args.nchans,args.nants, int(x.size/(args.nchans*args.nants*2*2)), 2, 2)
    x = x[:,:,0:int(x.size/((args.nchans*args.nants*2*2))),:,:].astype("float32").view("complex64")
    coherent_complex = x.sum(axis=1)

    incoherent = (abs(x)**2).sum(axis=1)
    incoherent_full = np.sum(incoherent, axis=2)


    incoherent_fin = np.vstack((incoherent_fin,incoherent_full[:,:,0]))
    coherent_fin = np.vstack((coherent_fin, coherent_complex[:,:,:,0]))

# Saving the coherent and incoherent beams
logging.info("Saving beams to file....")
np.save(args.basename + "_coherent.npy", coherent_fin[1::,:,:].astype("complex64"))
np.save(args.basename + "_incoherent.npy", incoherent_fin[1::,:].astype("float32"))
