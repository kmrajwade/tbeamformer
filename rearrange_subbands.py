#!/usr/env/python

# Script that rearranges the subbands and adds missing subbands to make the data frequency contiguous

import numpy as np
import logging
import argparse

# setup logging config

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)

# set up arguments
parser = argparse.ArgumentParser()
parser.add_argument("-f", "--freq", action='store', type=int, dest='freq',
    help="Number of frequency channels per subband")
parser.add_argument("-t", "--total", action='store', type=int, dest='nchans',
    help="Total number of channels")
parser.add_argument("-s", "--subbandid", action='store', type=str, dest='subbandlist',
    help="File containing the order of subband ids")
parser.add_argument("-i", "--indata", action='store', type=str, dest='tbdata',
    help="Input TB data. Should be a .npy or .npz file")


args = parser.parse_args()

# rearrange!
subbands = args.nchans/args.freq
subbandids = np.arange(0,subbands)

orig_subbandids = np.loadtxt(args.subbandlist)

data = np.load(args.tbdata)
samps = data['arr_0'].shape[1]
new_data = np.zeros((args.nchans,samps, 2 ), dtype='complex64')

for i in range(len(subbandids)):
       corr_idx = np.where(orig_subbandids == i)[0]
       if (len(corr_idx) == 0):
          logging.info("Subband %d does not exist. Filling 0s...." % i)
       else:
          logging.info("Correct subband is at index %d" % corr_idx)
          new_data[i*args.freq: (i+1)*args.freq,:,:] = data['arr_0'][corr_idx[0]*args.freq: (corr_idx[0]+1)*args.freq,:,:]

# Save the new data
np.save(args.tbdata.split(".")[0] + "_rearranged" + ".npy", new_data)

