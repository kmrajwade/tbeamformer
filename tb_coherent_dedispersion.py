#!/usr/env/python

# A script to run coherent dedispersion on TB data to remove intra-channel dispersion smear

import logging
import argparse
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sc


#setup logging configuration
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)

#setup arguments
parser = argparse.ArgumentParser(description = "A script to coherently dedisperse TB data")
parser.add_argument("-t", "--tsamp", action='store', type=float, dest='tsamp', required=True,
    help="Sampling interval (in seconds)")
parser.add_argument("-i", "--input", action='store', type=str, dest='inpfile', required=True,
    help="input beam file (.npy)")
parser.add_argument("-f", "--fftlen", action='store', type=int, dest='fftlen', required=True,
    help="length of fft to perform")
parser.add_argument("-l", "--lowfreq", action='store', type=float, dest='lowfreq', required=True,
    help="Lower edge of the frequency band (MHz)")
parser.add_argument("-b", "--bandwidth", action='store', type=float, dest='bw', required=True,
    help="Bandwidth (MHz)")
parser.add_argument("-d", "--dm", action='store', type=float, dest='dm', required=True,
    help="dispersion measure (pc/cc)")
parser.add_argument("-o", "--output", action='store', type=str, dest='output', required=True,
    help="output file name")


args = parser.parse_args()


# Dm delay

def dm_delay(nu1, nu2, dm, tsamp):
   return ((1/pow(nu1/1000,2.0)) - (1/pow(nu2/1000, 2.0)))*dm*0.00415/tsamp

# Take each frequency and fft it
data = np.load(args.inpfile)
nchan, nsamps, npol = data.shape
chanwidth = args.bw/nchan  # in MHz
channels = args.lowfreq + chanwidth*np.arange(0, nchan)
chanedges = args.lowfreq - chanwidth/2.0  + chanwidth*np.arange(0, nchan+1)
fftlen = args.fftlen

chirps = np.zeros((nchan, fftlen),dtype='complex64')
chirp_chan_width = chanwidth/fftlen # in MHz
f = np.linspace(-chanwidth/2.0, chanwidth/2.0, fftlen)
df = np.linspace(0, chanwidth, fftlen)

def chirp(flow, df, dm):
   s = (-2*np.pi*dm*4.148808e9)
   phase = (df * df * s)/((flow + df) * flow * flow)
   return (np.exp(1j*phase))

# define a window function
def hann(delay):
   x = np.zeros(int((fftlen - int(delay))))
   window = sc.windows.hann(delay)
   temp = np.append(window, x)
   return temp

logging.info("computing the chirp signal for all frequency channels...")
# generate chirp function
for i in range(nchan):
  fc = channels[i]
  fl = chanedges[i]
  # Apply the Hann window in time domain
  chirp_orig = chirp(fl, df, args.dm)
  ichirp = np.fft.ifft(chirp_orig)
  shift = int(dm_delay(chanedges[i], chanedges[i+1], args.dm, args.tsamp))
  hann_window = hann(shift)
  if (len(ichirp) > len(hann_window)):
     hann_window = np.pad(hann_window, (0,1), 'constant')
  if (len(ichirp) < len(hann_window)):
     hann_window = np.delete(hann_window, 0)
  modified_ichirp = ichirp * hann_window
  #generate the final chirp
  chirps[i,:] = np.fft.fftshift(np.fft.fft(modified_ichirp))
  

logging.info("computing the max shift from the dm...")
#generate max shifts for all channels
max_shift = int(dm_delay(chanedges[0], chanedges[1],args.dm, args.tsamp))

# check
if (fftlen < max_shift):
   logging.error("FFT length has to be greater than the max shift.")
   exit()

# run this over all time samples
iters = int(nsamps/fftlen)
first = True
spec = np.zeros((nchan, fftlen, npol))

# define output array
output = np.zeros((nchan, iters*(fftlen - max_shift), npol), dtype='complex64')

for i in range (iters):
   
   # run fft
   logging.info("Running the FFT...")
   if (first):
      spec = np.fft.fft(data[:, 0:fftlen,:], axis=1)
      first = False
   else:
      spec = np.fft.fft(data[:, i*(fftlen - max_shift): (i+1)*fftlen - i*max_shift,:], axis=1)
 
   spec[:, : , 0] *=  chirps
   spec[:, : , 1] *=  chirps

   # inverse Fourier transform
   logging.info("Inverting the spectrum....")
   codisp = np.fft.ifft(spec, axis = 1)

   #overlap and save
   logging.info("overlap and save....")
   correction=int(max_shift/2.0)
   if (int(max_shift/2.0)*2 != max_shift):
      correction = int(max_shift/2.0) + 1
    
   output[:,i*(fftlen - max_shift):(i+1)*(fftlen-max_shift),:] = codisp[:,int(max_shift/2.0): fftlen - correction, :]    

# save output as npy file
np.save(args.output + ".npy", output)


